% add paths
addpath(genpath('~/jsonlab/'));
addpath(genpath('~/V1_1_urlread2'));
addpath(genpath('~/sas-clientLib/src'));

% generate a sample dataList file
load('events_20160122.mat');
dataList = 'dataList';
fid = fopen(dataList,'w+');
for k =1:9
    fprintf(fid,'%s\n',events{k}.filename);
end
fprintf(fid,'%s',events{10}.filename);
fclose(fid);

% create Platt-scaled models
load('classLoss_2016-01-22-14-21-28-781_1.mat','svmModel');
SVMModel = svmModel{6}; % GCWA, see node-paper/sound2labId
if strcmp(SVMModel.ScoreTransform,'none')
    SVMModel = fitPosterior(SVMModel, SVMModel.X, SVMModel.Y);
end
%SVMModel = compact(SVMModel);
mSVMModel.alpha = SVMModel.Alpha;
mSVMModel.bias = SVMModel.Bias;
mSVMModel.kernelFcn = SVMModel.KernelParameters.Function;
mSVMModel.kernelScale = SVMModel.KernelParameters.Scale;
mSVMModel.mu = SVMModel.Mu;
mSVMModel.sigma = SVMModel.Sigma;
mSVMModel.sv = SVMModel.SupportVectors;
mSVMModel.svl = SVMModel.SupportVectorLabels;% score transform parameters
mSVMModel.ScoreTransform = SVMModel.ScoreTransform;
%save('GCWA_TFRSVM.mat','SVMModel');
save('GCWA_TFRSVM.mat','mSVMModel');

% test the m function
servAddr = 'acoustic.ifp.illinois.edu';
DB = 'publicDb';
PWD = 'publicPwd';
prob = GCWA_TFRSVM(dataList,servAddr,DB,PWD)

% test the binary function
%mcc -v -R -nodisplay -m GCWA_TFRSVM.m
sprintf('./run_GCWA_TFRSVM.sh /usr/local/MATLAB/MATLAB_Runtime/v85/ %s %s %s %s',dataList,servAddr,DB,PWD)