clear all; close all

fname = 'C:/Users/Long/Projects/ece544/proj/train.wav';
[x,fs] = audioread(fname);
Fs = 16e3;
x = resample(x,Fs,fs);
[S,F,T] = spectrogram(x,512,256,512,Fs);
Y = manLabel(S,F,T);

