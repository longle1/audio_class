function svmModel = quadTrainNASVM(svmModel,X,Y)
% svmModel = quadTrainNASVM(svmModel,X,Y)
%
% Long Le
% University of Illinois
%
tol = 1e-3;
nDat = size(X,1);

% init
svmModel = svmInit(svmModel,X,Y);

% find alpha
H = (svmModel.Y*svmModel.Y').*svmModel.K;
f = -ones(nDat,1);
A = [];
b = [];
Aeq = svmModel.Y';
beq = 0;
lb = zeros(nDat,1);
ub = svmModel.C*ones(nDat,1);
svmModel.alpha = quadprog(H,f,A,b,Aeq,beq,lb,ub);

% find bias using a support vector, any one will work
idx = find(svmModel.alpha > tol & svmModel.alpha < 1-tol);
wx = 0;
for k = 1:numel(svmModel.Y)
    wx = wx + svmModel.alpha(k)*svmModel.Y(k)*svmKernel( svmModel,svmModel.X(k,:),svmModel.X(idx(1),:) );
end
svmModel.bias = svmModel.Y(idx(1)) - wx;

end