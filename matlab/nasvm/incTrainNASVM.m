function [svmModelNew,nw,errCode] = incTrainNASVM(XAll, yAll, KAll, isDebug)
% function svmModelNew = incTrainNASVM(X, y, svmModel, isDebug)
% 
% X = NxD, N is the number of observations, D is observation dimension
% y = Nx1
% 
% Long Le <longle1@illinois.edu>
% University of Illinois
%

y0 = yAll(yAll==-1); X0 = XAll(yAll==-1,:);
y1 = yAll(yAll==1); X1 = XAll(yAll==1,:);
% initialize an svmModel with a minimum of 2 examples
svmModelMatlab = fitcsvm([X0(1,:);X1(1,:)],[y0(1);y1(1)]);
svmModel.sv = svmModelMatlab.SupportVectors;
svmModel.svl = svmModelMatlab.SupportVectorLabels;
svmModel.C = svmModelMatlab.BoxConstraints;
svmModel.alpha = svmModelMatlab.Alpha;
svmModel.bias = svmModelMatlab.Bias;
if exist('KAll','var') && ~isempty(KAll)
    svmModel.kernelFcn = 'noiseAdapt';
    
    K0 = KAll(yAll==-1);
    K1 = KAll(yAll==1,:);
    KRem = [K0(2:end,:);K1(2:end,:)];
    
    if svmModel.svl(1) == -1
        svmModel.svF{1,1} = inv(chol(K0{1},'lower'));
    elseif svmModel.svl(1) == 1
        svmModel.svF{1,1} = inv(chol(K1{1},'lower'));
    end
    if svmModel.svl(2) == -1
        svmModel.svF{2,1} = inv(chol(K0{1},'lower'));
    elseif svmModel.svl(2) == 1
        svmModel.svF{2,1} = inv(chol(K1{1},'lower'));
    end
else
    svmModel.kernelFcn = 'linear';
end
XRem = [X0(2:end,:);X1(2:end,:)];
yRem = [y0(2:end); y1(2:end)];
% random sampling
randIdx = randperm(size(XRem,1));
XRem = XRem(randIdx,:);
yRem = yRem(randIdx);

% incremental learning
svmModelNew = svmModel;
nw = zeros(size(XRem,1),1);
for k = 1:size(XRem,1)
    fprintf(1,'at example %d: X=[%.3f, %.3f],y=%d\n',k, XRem(k,1),XRem(k,2),yRem(k));
    
    nw(k) = norm(svmWeight(svmModelNew));
    if exist('KAll','var') && ~isempty(KAll)
        [svmModelNew,errCode] = incLearning(svmModelNew,XRem(k,:),yRem(k),isDebug,inv(chol(KRem{k},'lower')));
    else
        [svmModelNew,errCode] = incLearning(svmModelNew,XRem(k,:),yRem(k),isDebug);
    end
end

end

function [svmModelNew, errCode] = incLearning(svmModel,X,y,isDebug,F)
% function svmModelNew = incLearning(svmModel,X,y,isDebug,F)
% incrementally search for a new alpha
%
% Long Le <longle1@illinois.edu>
% University of Illinois
%
    svmModelNew = svmModel;
    errCode = 0;
    global eps;
    eps = 1e-6;
    MAX_ITER = 1e3;
    
    if isDebug
        plotSVProp(svmModelNew);
        suptitle('before')
    end

    % Append new sv to the old model
    needChange = [false(numel(svmModelNew.alpha),1); true];
    svmModelNew.sv = [svmModelNew.sv; X];
    svmModelNew.svl = [svmModelNew.svl; y];
    svmModelNew.C = [svmModelNew.C; 1];
    svmModelNew.alpha = [svmModelNew.alpha; 0]; % must change last, since this is in-place update
    if strcmp(svmModel.kernelFcn,'noiseAdapt')
        svmModelNew.svF = [svmModelNew.svF;{F}];
    end
    
    iter = 0;
    while any(needChange)
        % get a new chgIdx
        chgIdxSet = find(needChange); % current index to be changed
        if exist('chgIdx','var') && ~isempty(chgIdx)
            chgIdxSet = setdiff(chgIdxSet, chgIdx);
            if isempty(chgIdxSet)
                disp('failed to learn this data point: zero perturbation');
                errCode = 2;
                svmModelNew = svmModel; % reset to the original model
                return;
            end
        end
        chgIdx = chgIdxSet(1); % handle one point at a time, ignore the rest
        otherIdx = setdiff(1:numel(svmModelNew.alpha),chgIdx);
        while true
            iter = iter + 1;
            if iter > MAX_ITER
                disp('failed to learn this data point: max iteration reached');
                errCode = 1;
                svmModelNew = svmModel; % reset to the original model
                return;
            end

            if isReservedVector(svmModelNew,chgIdx,[otherIdx chgIdx])
                % drop out of support vector set
                needChange(chgIdx) = [];
                svmModelNew.sv(chgIdx,:) = [];
                svmModelNew.svl(chgIdx) = [];
                svmModelNew.C(chgIdx) = [];
                svmModelNew.alpha(chgIdx) = [];
                if strcmp(svmModel.kernelFcn,'noiseAdapt')
                    svmModelNew.svF(chgIdx) = [];
                end
                chgIdx = [];
                break;
            elseif isSupportVector(svmModelNew,chgIdx,[otherIdx chgIdx]) || isErrorVector(svmModelNew,chgIdx,[otherIdx chgIdx])
                needChange(chgIdx) = false;
                break;
            else
                % solve for the coefficient sensitivities that ensure existing KKT conditions
                % as a by-product, also get the resulting margin sensitivities
                [errCode,beta,perb] = computeAdiabaticParams(svmModelNew, chgIdx, otherIdx);
                if errCode == 1
                    disp('failed to learn this data point: singular matrix');
                    svmModelNew = svmModel; % reset to the original model
                    return;
                end
                if abs(perb) < eps
                    break; % nothing else can be done with the current vector
                end
                %{
                if errCode == 2
                    disp('failed to learn this data point: adiabatic deadlock');
                    svmModelNew = svmModel; % reset to the original model
                    return;
                end
                %}
                
                % ******** ADIABATIC CHANGE, i.e. increment/decrement
                svmModelNew.bias = svmModelNew.bias + beta(1)*perb;
                svmModelNew.alpha(otherIdx) = svmModelNew.alpha(otherIdx) + beta(2:end)*perb;
                svmModelNew.alpha(chgIdx) = svmModelNew.alpha(chgIdx) + perb;
                
                % check if the change remove any support vector
                for idx = otherIdx % these are support vectors
                    needChange(idx) = ~(isSupportVector(svmModelNew,idx,[otherIdx chgIdx]) || isErrorVector(svmModelNew,idx,[otherIdx chgIdx]));
                end
            end
        end
    end
    
    if isDebug
        plotSVProp(svmModelNew);
        suptitle('after')
    end
    
    disp('Learned a data point');
end

% compute adiabatic change's parameters
function [errCode,beta,perb] = computeAdiabaticParams(svmModel, chgIdx, otherIdx)
    global eps;
    errCode = 0; % no error
    
    zOtherIdx = zeros(size(otherIdx)); % g==0 other Idx
    for k = 1:numel(otherIdx)
        g = objDeriv(svmModel,otherIdx(k),[otherIdx chgIdx]);
        if abs(g)<eps;
            zOtherIdx(k) = otherIdx(k);
        end
    end
    zOtherIdx(zOtherIdx==0) = [];
    
    % ================================
    % Compute coeff sensitivities
    % ================================
    v = zeros(numel(zOtherIdx)+1,1);
    v(1,1) = svmModel.svl(chgIdx);
    for k = 1:numel(zOtherIdx)
        v(1+k,1) = quadTerm(svmModel,zOtherIdx(k),chgIdx);
    end
    
    Q = zeros(numel(zOtherIdx)+1,numel(otherIdx)+1);
    Q(1,:) = [0 svmModel.svl(otherIdx)'];
    for k = 1:numel(zOtherIdx)
        Q(1+k,1) = svmModel.svl(zOtherIdx(k));
        for l = 1:numel(otherIdx)
            Q(1+k,1+l) = quadTerm(svmModel,zOtherIdx(k),otherIdx(l));
        end
    end
    
    % TODO: handle rank deficiency due to duplicated sv
    % check if the matrix is singular
    if rank(Q) ~= size(Q,1)
        errCode = 1; % singular matrix error
    end
    
    beta = -Q\v;
    
    % ================================
    % Compute the optimal pertubation
    % ================================
    % Compute the margin sensitivity
    gamma = quadTerm(svmModel,chgIdx,chgIdx)+svmModel.svl(chgIdx)*beta(1);
    for k = 1:numel(otherIdx)
        gamma = gamma+quadTerm(svmModel,chgIdx,otherIdx(k))*beta(1+k);
    end
    
    gChgIdx = objDeriv(svmModel,chgIdx,[otherIdx chgIdx]);
    if abs(gamma) < eps % adiabatic update constraints lead to deadlock
        if gChgIdx > eps
            perb = -svmModel.alpha(chgIdx); % target 0
        elseif gChgIdx < -eps
            perb = svmModel.C(chgIdx)-svmModel.alpha(chgIdx); % target C
        else
            perb = svmModel.C(chgIdx)/2-svmModel.alpha(chgIdx); % target C/2
        end
    else
        perb = -gChgIdx/gamma;
    end
    % minimum feasible perb
    perbSet = max(-[svmModel.alpha(otherIdx);svmModel.alpha(chgIdx)],...
                min([svmModel.C(otherIdx);svmModel.C(chgIdx)]-[svmModel.alpha(otherIdx);svmModel.alpha(chgIdx);],...
                [beta(2:end);1]*perb))./[beta(2:end);1];
    [~,minIdx] = min(abs(perbSet));
    perb = perbSet(minIdx);
    
    % speed up for constraint-violated alpha
    %{
    if svmModel.alpha(chgIdx) < 0 || svmModel.alpha(chgIdx) > svmModel.C(chgIdx)
        perb = svmModel.C(chgIdx)/2-svmModel.alpha(chgIdx);
    end
    %}
end

% test for support vector
function status = isSupportVector(svmModel,idx,idxs)
    global eps;
    g = objDeriv(svmModel,idx,idxs);
    if abs(g)<eps && svmModel.alpha(idx)>=eps && svmModel.alpha(idx)<=svmModel.C(idx)-eps
        status = true;
    else
        status = false;
    end
end

% test for error vector
function status = isErrorVector(svmModel,idx,idxs)
    global eps;
    g = objDeriv(svmModel,idx,idxs);
    if (g<-eps || abs(g)<eps) && abs(svmModel.alpha(idx)-svmModel.C(idx))<eps
        status = true;
    else
        status = false;
    end
end

% test for reserved vector
function status = isReservedVector(svmModel,idx,idxs)
    global eps;
    g = objDeriv(svmModel,idx,idxs);
    if (g>eps || abs(g)<eps) && abs(svmModel.alpha(idx))<eps
        status = true;
    else
        status = false;
    end
end

% compute derivative of the objective function
function g = objDeriv(svmModel,idx,idxs)
    g = svmModel.svl(idx)*svmModel.bias - 1;
    for k = 1:numel(idxs)
        g = g + quadTerm(svmModel,idx,idxs(k))*svmModel.alpha(idxs(k));
    end
end

% quadratic term
function Q = quadTerm(svmModel,idx1,idx2)
    Q = svmModel.svl(idx1)*svmModel.svl(idx2)*svmKernel(svmModel,idx1,idx2);
end

% plot support vectors' properties for sanity check
% support vector should have g == 0 or alpha = C
function plotSVProp(svmModel)
    g = zeros(numel(svmModel.alpha),1);
    for k = 1:numel(svmModel.alpha)
        g(k) = objDeriv(svmModel,k,1:numel(svmModel.alpha));
    end
    figure; 
    subplot(211); plot(g); title('g');
    subplot(212); plot(svmModel.alpha); title('alpha');
end
