#!/bin/bash

for file in *.png; do
    convert "$file" $(echo "$file" | sed 's/\.png$/\.eps/')
    echo convert "$file" $(echo "$file" | sed 's/\.png$/\.eps/')
done
