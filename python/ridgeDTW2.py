import numpy as np
from scipy.stats.mstats import gmean
from pqdict import minpq
import itertools
import matplotlib.pyplot as plt

# subroutines
def spectrogram(y,fs,tBlk,tInc):
    nBlk = nFFT = 2**int(np.log2(int(tBlk*fs)))
    nInc = 2**int(np.log2(int(tInc*fs)))
    win = np.hanning(nBlk)
    
    tInc_quan = nInc / fs
    fInc_quan = fs / nFFT
    
    nT = int(np.ceil(len(y) / nInc))
    N = (nT - 1) * nInc + nBlk
    y_pad = np.pad(y,(0,N - (len(y))), 'constant', constant_values = 0)

    F = np.arange(nFFT//2)/nFFT*float(fs)
    T = np.arange(nT)*nInc/float(fs)
    S = np.empty((nFFT//2,nT))
    for k in range(nT):
        frame = y_pad[k*nInc : k*nInc + nBlk] * win
        spec = np.abs(np.fft.fft(frame, nFFT))
        S[:,k] = spec[0:nFFT//2]

    return S, F, T, fInc_quan, tInc_quan

def ridgeTracker(S,noiseFloorInit=2e2,btTime=32e-3,tInc=16e-3,isMaxPool=False,supThresh=5., noiseParams = [1.005, 1.005, 0.995]):
    btLen = int(btTime/tInc)
    alp = np.exp(-tInc/btTime)
    #print('btLen = %d' % btLen)
    #print('alp = %.4f' % alp)

    nF,nT = np.shape(S)
    snrOut = np.zeros((nF,nT))
    fOff = 2 # freq offset
    #supThresh = 2/(1-alp) # suppression threshold
    #print('supThresh = '+str(supThresh))

    noiseFloor = np.zeros((nF,nT+1))
    rUp = noiseParams[0]
    rUpSlow = noiseParams[1]
    rDown = noiseParams[2]

    snrAcc = np.zeros(nF)
    freAcc = np.arange(nF).astype(int)
    if np.size(noiseFloorInit) == 1:
        noiseFloor[:,0] = noiseFloorInit*np.ones(nF) 
    else:
        noiseFloor[:,0] = noiseFloorInit 
    ind = btLen*np.ones(nF) # indicator
    #btBuf = [None]*btLen # backtrack buffer
    for t in range(nT):
        # core DP 
        #bt = -np.ones(nF).astype(int) # backtrack
        snrAccLast = np.array(snrAcc)
        for f in range(nF):
            # noise update
            if S[f,t] > noiseFloor[f,t]:
                ind[f] -= 1
                if ind[f] < 0:
                    noiseFloor[f,t+1] = noiseFloor[freAcc[f],t]*rUp
                else:
                    noiseFloor[f,t+1] = noiseFloor[freAcc[f],t]*rUpSlow
            else:
                noiseFloor[f,t+1] = max(1e-6,noiseFloor[freAcc[f],t]*rDown)
                ind[f] = btLen

            # snr update
            snr = max(0.,2*np.log(S[f,t]/noiseFloor[f,t]))
            fLow = max(0,f-fOff)
            fHigh = min(nF,f+fOff+1)
            wWin = 1-0.05*np.abs(f-np.arange(fLow,fHigh))/fOff
            objFun = alp*snrAccLast[fLow:fHigh]+wWin*snr
            snrAcc[f] = np.max(objFun)
            freAcc[f] = fLow + np.argmax(objFun)
            #bt[f] = fLow + np.argmax(objFun)

        #del btBuf[0]
        #btBuf.append(bt)

        # max pooling
        if isMaxPool:
            snrAccLast = np.array(snrAcc)
            for f in range(nF):
                fLow = max(0,f-fOff)
                fHigh = min(nF,f+fOff+1)
                fun = snrAccLast[fLow:fHigh]
                if f == fLow+np.argmax(fun):
                    snrAcc[f] = np.max(fun)
                else:
                    snrAcc[f] = 0

        # per-bin suppressed stats
        for f in range(nF):
            if snrAcc[f] > supThresh:
                '''
                fBt = f
                for k in reversed(range(btLen)):
                    if btBuf[k] is not None and btBuf[k][fBt] != -1:
                        fBt = btBuf[k][fBt]
                    else:
                        break

                snrOut[fBt,t-(btLen-k)] = snrAcc[f]-supThresh
                '''
                snrOut[f,t-btLen//2] = snrAcc[f]-supThresh

    return snrOut,noiseFloor

def dtw(Xs,Ns,axis,eps=1,debug=False):
    return ridgeFusion(Xs,Ns,axis,eps,debug)
    
def ridgeFusion(Xs,Ns,axis,eps=1,debug=False):
    M = len(Xs)

    # replaced 0 SNR with min SNR
    minSNR = min([min(Xs[k].flatten()[np.flatnonzero(Xs[k])]) for k in range(M)])
    for k in range(M):
        Xs[k][np.where(Xs[k]==0)] = minSNR

    nTs = np.zeros(M)
    for k in range(M):
        nF,nTs[k] = np.shape(Xs[k])
        
    start = tuple(np.zeros(M).astype(int))
    goal = tuple(nTs-np.ones(M).astype(int))

    #tStartA = time.clock()
    fullPath,minDist,explored = Astar(Xs,Ns,nTs,start,goal,eps,debug)
    #print('tElapsedA = '+str(time.clock() - tStartA))

    #tStartW = time.clock()
    Xp,Np,c = lhsWarped(fullPath,Xs,Ns,axis)
    #print('tElapsedW = '+str(time.clock() - tStartW))

    if debug:
        print('minDist = '+str(minDist))
        #print('np.sum(eDist) = '+str(np.sum(eDist)))
        if M == 2:
            plt.figure()
            x = []
            y = []
            for node in explored:
                y.append(node[0]+0.5)
                x.append(node[1]+0.5)
            plt.plot(np.array(x),np.array(y),color='green',marker='x',linestyle='None')

            x = []
            y = []
            for node in fullPath:
                y.append(node[0]+0.5)
                x.append(node[1]+0.5)
            plt.plot(np.array(x),np.array(y),color='red')
            plt.xlabel('Frame index')
            plt.ylabel('Frame index')
            #plt.savefig('../doc/optPath.png', bbox_inches='tight', pad_inches=0.1)
            plt.show()

    # warped signal, total cost, per-frame cost
    return Xp,Np,c 

def Astar(Xs,Ns,nTs,start,goal,eps=1,debug=False):
    #depth = {}
    dist = {}
    prev = {}
    frontier = minpq()
    explored = set()
    #S = []

    cnt = 0

    #depth[start] = 1
    #frontier[start] = 0+getHeuristic(start,goal)*(1+eps*(1-depth[start]/500))
    #print('len(goal) = %s' % len(goal))
    #print('len(Xs) = %s' % len(Xs))
    frontier[start] = 0+eps*getHeuristic(start,goal)
    dist[start] = 0
    while len(frontier) > 0:
        #node,prio = frontier.popitem()
        node = frontier.pop()

        
        if debug and cnt % 10000 == 0:
            #print('dist[node] = '+str(dist[node]))
            #print('depth[node] = '+str(depth[node]))
            #fullPath = getPath(node,prev)
            #print('len(fullPath) = '+str(len(fullPath)))
            print('node = '+str(node))
        cnt += 1

        if node == goal:
            return getPath(node,prev),dist[node],explored

        explored.add(node)

        #tStartN = time.clock()
        for ngb in getNeighbor(node,nTs):
            if ngb not in explored:
                if ngb not in frontier:
                    frontier[ngb] = np.infty
                    dist[ngb] = np.infty
                    #depth[ngb] = np.infty

                #tStartD = time.clock()
                alt = dist[node] + getDist(node,ngb,Xs,Ns)
                #print('tElapsedD = '+str(time.clock() - tStartD))
                if alt < dist[ngb]:
                    #depth[ngb] = depth[node]+1
                    #frontier[ngb] = alt+getHeuristic(ngb,goal)*(1+eps*(1-depth[ngb]/500))
                    frontier[ngb] = alt+eps*getHeuristic(ngb,goal)
                    dist[ngb] = alt
                    prev[ngb] = node

        #print('tElapsedN = '+str(time.clock() - tStartN))

        '''
        while True:
            _,prioNext = frontier.topitem()
            if prioNext == prio:
                S.append(frontier.popitem())
            else:
                break

        if len(frontier) == 0 and len(S) > 0:
            print('DFS on equal-length paths')
            nodeNew,prioNew = S.pop()
            frontier[nodeNew] = prioNew
        '''

    return None,None,explored

def getPath(cur,prev):
    fullPath = []
    fullPath.append(cur)
    while cur in prev:
        cur = prev[cur]
        fullPath.append(cur)

    return fullPath

def getWeight(u,v,Xs):
    M = len(Xs)
    vecDiff = np.array(v)-np.array(u)
    return np.linalg.norm(vecDiff,ord=1)/2/M

# ensure that the heuristic is 
# admissible and monotone/consistent
def getHeuristic(u,v):
    M = len(u)
    return euclidDist(u,v)/np.sqrt(M)

def getDist(u,v,Xs,Ns):
    # local distance only
    return getHeuristic(u,v) + 1-evalSim(v,Xs,Ns)

def getNeighbor(node,nTs):
    M = len(node)

    neighbor = []
    for d in getDelta(M):
        n = tuple(np.array(node)+np.array(d))
        if globalCon(n,nTs):
            neighbor.append(n)

    return neighbor

def getDelta(M):
    delta1 = list(itertools.product([0,1],repeat=M))
    del delta1[0]
    del delta1[-1]

    delta2 = list(itertools.product([0,1],repeat=M))
    del delta2[-1]
    for k in range(len(delta2)):
        delta2[k] = tuple(np.array(delta2[k])+1)

    return delta1+delta2

def lhsWarped(fullPath,Xs,Ns,axis):
    M = len(Xs)
    Xp = np.zeros(np.shape(Xs[axis]))
    Np = np.zeros(np.shape(Ns[axis]))
    nodePrev = tuple(-np.ones(M).astype(int))
    for node in reversed(fullPath):
        Xr = rhsWarped(axis,node,Xs)
        Nr = rhsWarped(axis,node,Ns)
        if node[axis] - nodePrev[axis] == 0:
            pass
        elif node[axis] - nodePrev[axis] == 1:
            Xp[:,node[axis]] = Xr
            Np[:,node[axis]] = Nr
        elif node[axis] - nodePrev[axis] == 2:
            Xp[:,node[axis]] = Xr 
            Xp[:,node[axis]-1] = Xr
            Np[:,node[axis]] = Nr 
            Np[:,node[axis]-1] = Nr

        nodePrev = node

    # compute the per-frame cost
    _,nT = np.shape(Xp)
    c = np.zeros(nT)
    for k in range(nT):
        node = (k,k)
        c[k] = evalSim(node,[Xs[axis],Xp],[Ns[axis],Np])

    # compute the per-frame Euclidean dist 
    '''
    eDist = []
    nodePrev = tuple(np.zeros(M).astype(int))
    for node in reversed(fullPath):
        eDist.append(euclidDist(node,nodePrev))
        nodePrev = node
    eDist = np.array(eDist)
    '''

    return Xp,Np,c

def rhsWarped(axis,node,Xs):
    M = len(Xs)

    # include frames from all templates
    Xr = np.array([Xs[k][:,node[k]] for k in range(M) if k != axis]).T
    #print('Xr.shape = %s' % (Xr.shape,))

    return gmean(Xr,axis=1)

def evalSim(node,Xs,Ns):
    # evaluate similarity among the Xs at 
    # a given time node
    M = len(Xs)
    
    #print('node = %s' % (node,))
    Xt = np.array([Xs[k][:,node[k]] for k in range(M)]).T
    Nt = np.array([Ns[k][:,node[k]] for k in range(M)]).T

    den = np.prod(np.linalg.norm(Xt,axis=0))
    return np.sum(np.prod(Xt,axis=1))/max(1, den)

def globalCon(node,nTs):
    M = len(node)

    tmp = np.array(node)
    tmp.sort()
    #rv = (tmp[-1]-tmp[0] <= np.mean(nTs)/2)
    rv = True
    for k in range(M):
        rv = rv and (node[k] < nTs[k]) and (node[k] >= 0)
    
    return rv

def euclidDist(u,v):
    return np.linalg.norm(np.array(v)-np.array(u))
