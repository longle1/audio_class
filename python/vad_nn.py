#!/usr/bin/env python
#
# Long Le <longle2718@gmail.com>
#

## Utility functions for rnn.ipynb

import numpy as np
import torch
from torch.autograd import Variable
import torch.nn as nn

D_IN, H, D_OUT, L = 36, 200, 1, 1

class VAD_LSTM(nn.Module):
    def __init__(self):
        super(VAD_LSTM, self).__init__()
        # input data dim, hidden dim, # of layers
        self.lstm = nn.LSTM(D_IN, H, L, batch_first=True)
        # output dim
        self.linear = nn.Linear(H, D_OUT)

    def forward(self, feat_trun, h_0=None, c_0=None):
        N,T = feat_trun.shape[0],feat_trun.shape[1]
        
        # num_layers, batch, hidden_size
        # input/init conditions do not require grad
        if h_0 is None:
            h_0 = Variable(torch.zeros(L,N,H).type(feat_trun.data.type()), requires_grad=False)
        if c_0 is None:
            c_0 = Variable(torch.zeros(L,N,H).type(feat_trun.data.type()), requires_grad=False)
        
        # input/output: batch, seq, feature
        h_all,(h_n,c_n) = self.lstm(feat_trun,(h_0,c_0))
        
        outputs = []
        for h_t in torch.chunk(h_all,h_all.size(1),dim=1):
            output = self.linear(torch.squeeze(h_t,dim=1))
            outputs += [output]
        # stack along seq/time, inserted as dim 1, 
        # thus: batch, seq, out dim
        outputs = torch.stack(outputs, 1)
        
        return outputs,h_n,c_n

def getArchParams():
    return D_IN, H, D_OUT, L

def len_pad(inlist):
    # save the len of all elements in the input list
    # before padding them
    lens = np.zeros(len(inlist),dtype=int)
    for k in range(len(inlist)):
        lens[k] = len(inlist[k])

    maxlen = max(lens)
    outlist = [None]*len(inlist)
    for k in range(len(inlist)):
        outlist[k] = np.pad(inlist[k], ((0,maxlen-len(inlist[k])),) + ((0,0),)*(len(inlist[k].shape)-1), mode='constant')
    
    # sort in decreasing len
    sDict = sorted(enumerate(lens), key=lambda i:i[1], reverse=True)
    lens = [i[1] for i in sDict]
    outlist = [outlist[i[0]] for i in sDict]
    return lens,outlist
