'''
Centroid-based dynamic time-warp classifier

Long Le <longle1@illinois.edu>
University of Illinois
'''

import numpy as np
from scipy import signal
from scipy.special import expit
from NoiseAdapter import NoiseAdapter
from DTWScorer import DTWScorer

class CentroidDTWClassifier:
    def __init__(self,nBlk=512,nInc=256,fs=16000,naMinVal=1e-8,naSigDur=5,naIniVal=1e-1):
        self.templates = []
        self.nBlk = nBlk
        self.nInc = nInc
        self.fs = fs
        self.naMinVal = naMinVal
        self.naSigDur = naSigDur
        self.naIniVal = naIniVal
        self.X = [] # input prob spectrum

        return

        def dynamic_centroids(self,x,M):
            ''' 
            Compute dynamic centroids
            Input
                    x: a vector of value space for centroid
                    M: a sequence of vectors of mass over the same value space
            Output
                    cent: centroids
                    cent_foff_up: upper bounds of the centroids
                    cent_foff_down: lower bounds of the centroids
            '''
            nT = M.shape[0]
            nF = M.shape[1]
            cent = np.zeros((nT,nF)) # centroid
            cent_foff_up = np.zeros((nT,nF)) # centroid upper bound
            cent_foff_down = np.zeros((nT,nF)) # centroid lower bound
            for ti in range(nT):
                m = list(M[ti,:])
                cnt = 0
                while cnt < 3:
                    aCent = self.binSearchCent(x,m)
                    if aCent == None:
                        break
                    up_bd,low_bd = self.centBd(m,aCent)

                    # suppress the known centroid
                    m[low_bd:up_bd+1] = [0]*(up_bd+1-low_bd)

                    cent[ti,aCent] = 1
                    cent_foff_down[ti,low_bd] = 1
                    cent_foff_up[ti,up_bd] = 1

                    cnt += 1

            return cent,cent_foff_up,cent_foff_down 

        def extractFeats(self,data):
            rad = 4
            scale = 1e1

            f,t,S = signal.spectrogram(data,self.fs,'hann',self.nBlk,self.nBlk-self.nInc)
            S = S.T

            nBank = S.shape[1]
            na = NoiseAdapter(nBank,self.naMinVal,self.naSigDur,self.naIniVal)
            noisefloor = na.track(S)

            X = expit((S-noisefloor*10)*scale)

            pS = np.zeros(X.shape)
            for ti in range(len(X)):
                pS[ti,:] = self.smooth(X[ti,:],rad)

            #F = np.arange(np.shape(pS)[1])
            #cent,cent_foff_up,cent_foff_down = self.dynamic_centroids(F,pS)
            #X = np.c_[cent,cent_foff_up,cent_foff_down]

            return pS

        def simplexInc(self,w,i,delta):
            '''
            Increment in the probability simplex (PS)

            Input
            :w: vector in the PS
            :i: index of the move
            :delta: length of the move

            Output
            :wNew: the moved vector
            '''
            wNew = np.array(w)
            # ensure that the increment is within the simplex
            delta = min(delta, 1-max(wNew))
            delta = min(delta, min(wNew)*(len(w)-1))
            for k in range(len(w)):
                if k == i:
                    wNew[k] += delta
                else:
                    wNew[k] -= delta/(len(w)-1)
            return wNew

        def mergeFeats(self,w0,w1,X0,X1,allPos):
            X = []
            for ti in range(1,len(allPos)):
                #if not (allPos[ti,0] == allPos[ti-1,0] and allPos[ti,1] == allPos[ti-1,1]):
                X += [w0*X0[allPos[ti,0],:]+w1*X1[allPos[ti,1],:]]
            X = np.array(X) 
            X = signal.resample(X,(len(X0)+len(X1))//2)
            return X

        def evalFeat(self,w,X):
            if len(X) > 2:
                w0 = np.sum(w[:len(X)//2])
                X0 = self.evalFeat(w[:len(X)//2]/w0,X[:len(X)//2])
                w1 = np.sum(w[len(X)//2:])
                X1 = self.evalFeat(w[len(X)//2:]/w1,X[len(X)//2:])
            else:
                w0 = w[0]
                X0 = X[0]
                w1 = w[1]
                X1 = X[1]

            _,_,_,allPos,_ = DTWScorer().computeScore(X0,X1)
            X0 = self.mergeFeats(w0,w1,X0,X1,allPos)
            return X0

        def evalLoss(self,w):
            L = 0
            X = self.X
            X0 = self.evalFeat(w,X)
            for idx in range(len(X)):
                s,_,_,_,_ = DTWScorer().computeScore(X0,X[idx])
                L += s
            return L	

        def fit(self,dataSet):
            delta = 0.03

            if len(dataSet) % 2 != 0:
                raise Exception('The dataset\'s size must be an even number')

            #self.X = []
            for idx in range(len(dataSet)):
                self.X += [self.extractFeats(dataSet[idx])]

            w = np.ones(len(dataSet))/len(dataSet)
            while True:
                L = self.evalLoss(w)
                print('w = '+str(w)+', L = '+str(L))

                wNext = []
                LNext = []
                for idx in range(len(w)):
                    wNext += [self.simplexInc(w,idx,delta)]
                    LNext += [self.evalLoss(wNext[idx])]
                if any(LNext > L):
                    w = wNext[np.argmax(LNext-L)]
                else:
                    print('Training finished.')
                    break

            X = self.X
            X0 = self.evalFeat(w,X)
            self.templates = [X0]
            '''
            for idx in range(1,len(dataSet)):
                    X1 = self.extractFeats(dataSet[idx])
                    s,V,lnk,allPos,_ = DTWScorer().computeScore(X0,X1)
                    if s > 0.5:
                            X = []
                            for ti in range(1,len(allPos)):
                                    if not (allPos[ti,0] == allPos[ti-1,0] and allPos[ti,1] == allPos[ti-1,1]):
                                            X += [(X0[allPos[ti,0],:]+X1[allPos[ti,1],:])/2]
                            X = np.array(X) 
                            X = signal.resample(X,(len(X0)+len(X1))//2)

                            self.templates = [X]
                            X0 = X
                    else:
                            self.templates += [X1]
            '''

            return LNext

        def predict(self,data):
            X = self.extractFeats(data)

            Tscores = [] # scores per templates
            for X0 in self.templates:
                nBlk = len(X0)
                nInc = len(X0)//2

                scores = [] # scores over frames
                curIdx = 0
                while curIdx < len(X):
                    print('['+str(curIdx)+':'+str(min([curIdx+nBlk,len(X)]))+']')
                    #print(curIdx)
                    #print(curIdx+nBlk)
                    #print(len(X))
                    X1 = X[curIdx:min([curIdx+nBlk,len(X)]),:]
                    curIdx += nInc
                    s,_,_,_,_ = DTWScorer().computeScore(X0,X1)
                    scores += [s]
                Tscores += [scores]

            return np.nanmax(np.array(Tscores),axis=0)

        def centroid(self,x,m):
            eps = 1e-10
            den = np.sum(m)
            if den == 0:
                m[int(round(np.random.random()*(len(m)-1)))] = eps

            num = np.sum(x*m)
            den = np.sum(m)

            return num/den

        def binSearchCent(self,x,m):
            #dens = density(m)

            x0 = x
            m0 = m
            while True:
                aCent = int(round(self.centroid(x0,m0)))
                '''
                try:
                        aCent = int(round(centroid(x0,m0)))
                except ValueError:
                        print('x0 = '+str(x0))
                        print('m0 = '+str(m0))
                        raise ValueError('debugging')
                '''

                if len(m0) < 2:
                    '''
                    print(x0)
                    print(aCent)
                    print(m[aCent+1])
                    '''
                    return None

                #thresh = np.percentile(m,80)
                #if dens[aCent] >= thresh:
                # check if the centroid has a significant mass
                win = (x0[len(x0)-1] - x0[0])//6
                if m[aCent] >= 0.8 and aCent >= x0[0]+win and aCent <= x0[len(x0)-1]-win:
                    return aCent

                if aCent >= x0[len(x0)//2]:
                    x0 = x0[len(x0)//2:]
                    m0 = m0[len(m0)//2:]
                else:
                    x0 = x0[:len(x0)//2]
                    m0 = m0[:len(m0)//2]

        def centBd(self,m,aCent):
            up_bd = aCent
            low_bd = aCent
            while np.mean(m[aCent:up_bd+1]) >= 0.5 and up_bd < len(m)-1:
                up_bd += 1
            while np.mean(m[low_bd:aCent+1]) >= 0.5 and low_bd > 0:
                low_bd -= 1

            return up_bd,low_bd

        def smooth(self,x,rad):
            y = np.zeros(len(x))
            for k in range(len(x)):
                y[k] = np.mean(x[max(0,k-rad):min(len(x),k+rad)])
            return y


if __name__ == '__main__':
    print(__doc__)
