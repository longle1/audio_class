'''
Ridge-based DTW

Long Le <longle1@illinois.edu>
University of Illinois
'''

import sys,os
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
from scipy.stats.mstats import gmean
import pickle
import audio_class
import itertools
import time
from pqdict import minpq
from joblib import Parallel,delayed

# certain Python modules (numpy, scipy, tables, pandas, skimage...) 
# mess with core affinity on import, here's a workaround
os.system("taskset -p 0xff %d" % os.getpid())

'''
def noiseAdapter(S,btTime=32e-3,noiseFloorInit=2e2):
    nF,nT = np.shape(S)
    noiseFloor = np.zeros((nF,nT+1))
    rUp = 1.01
    rUpSlow = 1.005
    rDown = 0.99
    btLen = int(btTime/tInc)
    print('btLen = %d' % btLen)

    noiseFloor[:,0] = noiseFloorInit*np.ones(nF) 
    ind = btLen*np.ones(nF) # indicator
    for t in range(nT):
        for f in range(nF):
            if S[f,t] > noiseFloor[f,t]:
                ind[f] -= 1
                if ind[f] < 0:
                    noiseFloor[f,t+1] = noiseFloor[f,t]*rUp
                else:
                    noiseFloor[f,t+1] = noiseFloor[f,t]*rUpSlow
            else:
                noiseFloor[f,t+1] = max(1e-6,noiseFloor[f,t]*rDown)
                ind[f] = btLen

    return noiseFloor
'''

def AIgramTFR(filename,plot=False):
    fs,data = audio_class.readSegment(filename)

    S,ff,tt,tBlk,tInc = audio_class.spectrographic(data,fs,32e-3,16e-3)
    X,_ = ridgeTracker(S,np.median(S),32e-3,16e-3)

    if plot:
        plt.figure()
        plt.pcolormesh(tt,ff,np.sqrt(X))
        plt.autoscale(True,'both',True)
        plt.xlabel('Time (s)')
        plt.ylabel('Frequency (Hz)')
        plt.show()

    return X

def ridgeTracker(S,noiseFloorInit=2e2,btTime=32e-3,tInc=16e-3,isMaxPool=False,supThresh=5.):
    btLen = int(btTime/tInc)
    alp = np.exp(-tInc/btTime)
    #print('btLen = %d' % btLen)
    #print('alp = %.4f' % alp)

    nF,nT = np.shape(S)
    snrOut = np.zeros((nF,nT))
    fOff = 2 # freq offset
    #supThresh = 2/(1-alp) # suppression threshold
    #print('supThresh = '+str(supThresh))

    noiseFloor = np.zeros((nF,nT+1))
    rUp = 1.01
    rUpSlow = 1.005
    rDown = 0.99

    snrAcc = np.zeros(nF)
    freAcc = np.arange(nF).astype(int)
    if np.size(noiseFloorInit) == 1:
        noiseFloor[:,0] = noiseFloorInit*np.ones(nF) 
    else:
        noiseFloor[:,0] = noiseFloorInit 
    ind = btLen*np.ones(nF) # indicator
    #btBuf = [None]*btLen # backtrack buffer
    for t in range(nT):
        # core DP 
        #bt = -np.ones(nF).astype(int) # backtrack
        snrAccLast = np.array(snrAcc)
        for f in range(nF):
            # noise update
            if S[f,t] > noiseFloor[f,t]:
                ind[f] -= 1
                if ind[f] < 0:
                    noiseFloor[f,t+1] = noiseFloor[freAcc[f],t]*rUp
                else:
                    noiseFloor[f,t+1] = noiseFloor[freAcc[f],t]*rUpSlow
            else:
                noiseFloor[f,t+1] = max(1e-6,noiseFloor[freAcc[f],t]*rDown)
                ind[f] = btLen

            # snr update
            snr = max(0.,2*np.log(S[f,t]/noiseFloor[f,t]))
            fLow = max(0,f-fOff)
            fHigh = min(nF,f+fOff+1)
            wWin = 1-0.05*np.abs(f-np.arange(fLow,fHigh))/fOff
            objFun = alp*snrAccLast[fLow:fHigh]+wWin*snr
            snrAcc[f] = np.max(objFun)
            freAcc[f] = fLow + np.argmax(objFun)
            #bt[f] = fLow + np.argmax(objFun)

        #del btBuf[0]
        #btBuf.append(bt)

        # max pooling
        if isMaxPool:
            snrAccLast = np.array(snrAcc)
            for f in range(nF):
                fLow = max(0,f-fOff)
                fHigh = min(nF,f+fOff+1)
                fun = snrAccLast[fLow:fHigh]
                if f == fLow+np.argmax(fun):
                    snrAcc[f] = np.max(fun)
                else:
                    snrAcc[f] = 0

        # per-bin suppressed stats
        for f in range(nF):
            if snrAcc[f] > supThresh:
                '''
                fBt = f
                for k in reversed(range(btLen)):
                    if btBuf[k] is not None and btBuf[k][fBt] != -1:
                        fBt = btBuf[k][fBt]
                    else:
                        break

                snrOut[fBt,t-(btLen-k)] = snrAcc[f]-supThresh
                '''
                snrOut[f,t-btLen//2] = snrAcc[f]-supThresh

    return snrOut,noiseFloor

def blockProc(allS,bWin,bInc,fs,btTime,tInc):
    nF,nT = np.shape(allS)

    # params
    with open('log/template_ridge.pkl','rb') as fid:
        X_h,N_h = pickle.load(fid)

    # block processing
    nB = int(np.ceil(nT/bInc)) # number of blocks (of frames)
    blks = Parallel(n_jobs=8)(delayed(blockProcPar)(k,nB,bInc,bWin,nT,allS,btTime,tInc,X_h,N_h) for k in range(nB))
    blks = np.array(blks)
    
    '''
    print('np.shape(blks) = '+str(np.shape(blks)))
    k = 8
    plt.figure()
    plt.plot(blks[k,:])
    plt.title('np.sum(blks[k,:]) = '+str(np.sum(blks[k,:])))
    plt.show()
    '''

    out = audio_class.blks2frames(blks,bWin,bInc)

    return out[:nT]

def blockProcPar(k,nB,bInc,bWin,nT,allS,btTime,tInc,X_h,N_h):
    sys.stdout.write("Progress: %d%%   \r" % (k/nB*100.) )
    frameL = k*bInc
    frameH = min(nT,k*bInc+bWin)
    S = np.pad(allS[:,frameL:frameH],((0,0),(0,bWin-(frameH-frameL))),'constant',constant_values=1e-6)
    # convert from S (spectrogram) to X (ridge)
    X,N = ridgeTracker(S,np.median(S),btTime,tInc)
    # warp to the dim of X
    _,_,c = dtw([X_h,X],[N_h,N],1)

    return c

def dtw(Xs,Ns,axis,eps=1,debug=False):
    return ridgeFusion(Xs,Ns,axis,eps,debug)
    
def ridgeFusion(Xs,Ns,axis,eps=1,debug=False):
    M = len(Xs)

    # replaced 0 SNR with min SNR
    minSNR = min([min(Xs[k].flatten()[np.flatnonzero(Xs[k])]) for k in range(M)])
    for k in range(M):
        Xs[k][np.where(Xs[k]==0)] = minSNR

    nTs = np.zeros(M)
    for k in range(M):
        nF,nTs[k] = np.shape(Xs[k])
        
    start = tuple(np.zeros(M).astype(int))
    goal = tuple(nTs-np.ones(M).astype(int))

    #tStartA = time.clock()
    fullPath,minDist,explored = Astar(Xs,Ns,nTs,start,goal,eps,debug)
    #print('tElapsedA = '+str(time.clock() - tStartA))

    #tStartW = time.clock()
    Xp,Np,c = lhsWarped(fullPath,Xs,Ns,axis)
    #print('tElapsedW = '+str(time.clock() - tStartW))

    if debug:
        print('minDist = '+str(minDist))
        #print('np.sum(eDist) = '+str(np.sum(eDist)))
        if M == 2:
            plt.figure()
            x = []
            y = []
            for node in explored:
                y.append(node[0]+0.5)
                x.append(node[1]+0.5)
            plt.plot(np.array(x),np.array(y),color='green',marker='x',linestyle='None')

            x = []
            y = []
            for node in fullPath:
                y.append(node[0]+0.5)
                x.append(node[1]+0.5)
            plt.plot(np.array(x),np.array(y),color='red')
            plt.xlabel('Frame index')
            plt.ylabel('Frame index')
            #plt.savefig('../doc/optPath.png', bbox_inches='tight', pad_inches=0.1)
            plt.show()

    # warped signal, total cost, per-frame cost
    return Xp,Np,c 

def Astar(Xs,Ns,nTs,start,goal,eps=1,debug=False):
    #depth = {}
    dist = {}
    prev = {}
    frontier = minpq()
    explored = set()
    #S = []

    cnt = 0

    #depth[start] = 1
    #frontier[start] = 0+getHeuristic(start,goal)*(1+eps*(1-depth[start]/500))
    #print('len(goal) = %s' % len(goal))
    #print('len(Xs) = %s' % len(Xs))
    frontier[start] = 0+eps*getHeuristic(start,goal)
    dist[start] = 0
    while len(frontier) > 0:
        #node,prio = frontier.popitem()
        node = frontier.pop()

        
        if debug and cnt % 10000 == 0:
            #print('dist[node] = '+str(dist[node]))
            #print('depth[node] = '+str(depth[node]))
            #fullPath = getPath(node,prev)
            #print('len(fullPath) = '+str(len(fullPath)))
            print('node = '+str(node))
        cnt += 1

        if node == goal:
            return getPath(node,prev),dist[node],explored

        explored.add(node)

        #tStartN = time.clock()
        for ngb in getNeighbor(node,nTs):
            if ngb not in explored:
                if ngb not in frontier:
                    frontier[ngb] = np.infty
                    dist[ngb] = np.infty
                    #depth[ngb] = np.infty

                #tStartD = time.clock()
                alt = dist[node] + getDist(node,ngb,Xs,Ns)
                #print('tElapsedD = '+str(time.clock() - tStartD))
                if alt < dist[ngb]:
                    #depth[ngb] = depth[node]+1
                    #frontier[ngb] = alt+getHeuristic(ngb,goal)*(1+eps*(1-depth[ngb]/500))
                    frontier[ngb] = alt+eps*getHeuristic(ngb,goal)
                    dist[ngb] = alt
                    prev[ngb] = node

        #print('tElapsedN = '+str(time.clock() - tStartN))

        '''
        while True:
            _,prioNext = frontier.topitem()
            if prioNext == prio:
                S.append(frontier.popitem())
            else:
                break

        if len(frontier) == 0 and len(S) > 0:
            print('DFS on equal-length paths')
            nodeNew,prioNew = S.pop()
            frontier[nodeNew] = prioNew
        '''

    return None,None,explored

def getPath(cur,prev):
    fullPath = []
    fullPath.append(cur)
    while cur in prev:
        cur = prev[cur]
        fullPath.append(cur)

    return fullPath

def getWeight(u,v,Xs):
    M = len(Xs)
    vecDiff = np.array(v)-np.array(u)
    return np.linalg.norm(vecDiff,ord=1)/2/M

# ensure that the heuristic is 
# admissible and monotone/consistent
def getHeuristic(u,v):
    M = len(u)
    return euclidDist(u,v)/np.sqrt(M)

def getDist(u,v,Xs,Ns):
    # local distance only
    return getHeuristic(u,v) + 1-evalSim(v,Xs,Ns)

def getNeighbor(node,nTs):
    M = len(node)

    neighbor = []
    for d in getDelta(M):
        n = tuple(np.array(node)+np.array(d))
        if globalCon(n,nTs):
            neighbor.append(n)

    return neighbor

def getDelta(M):
    delta1 = list(itertools.product([0,1],repeat=M))
    del delta1[0]
    del delta1[-1]

    delta2 = list(itertools.product([0,1],repeat=M))
    del delta2[-1]
    for k in range(len(delta2)):
        delta2[k] = tuple(np.array(delta2[k])+1)

    return delta1+delta2

def lhsWarped(fullPath,Xs,Ns,axis):
    M = len(Xs)
    Xp = np.zeros(np.shape(Xs[axis]))
    Np = np.zeros(np.shape(Ns[axis]))
    nodePrev = tuple(-np.ones(M).astype(int))
    for node in reversed(fullPath):
        Xr = rhsWarped(axis,node,Xs)
        Nr = rhsWarped(axis,node,Ns)
        if node[axis] - nodePrev[axis] == 0:
            pass
        elif node[axis] - nodePrev[axis] == 1:
            Xp[:,node[axis]] = Xr
            Np[:,node[axis]] = Nr
        elif node[axis] - nodePrev[axis] == 2:
            Xp[:,node[axis]] = Xr 
            Xp[:,node[axis]-1] = Xr
            Np[:,node[axis]] = Nr 
            Np[:,node[axis]-1] = Nr

        nodePrev = node

    # compute the per-frame cost
    _,nT = np.shape(Xp)
    c = np.zeros(nT)
    for k in range(nT):
        node = (k,k)
        c[k] = evalSim(node,[Xs[axis],Xp],[Ns[axis],Np])

    # compute the per-frame Euclidean dist 
    '''
    eDist = []
    nodePrev = tuple(np.zeros(M).astype(int))
    for node in reversed(fullPath):
        eDist.append(euclidDist(node,nodePrev))
        nodePrev = node
    eDist = np.array(eDist)
    '''

    return Xp,Np,c

def rhsWarped(axis,node,Xs):
    M = len(Xs)

    # include frames from all templates
    Xr = np.array([Xs[k][:,node[k]] for k in range(M) if k != axis]).T
    #print('Xr.shape = %s' % (Xr.shape,))

    return gmean(Xr,axis=1)

def evalSim(node,Xs,Ns):
    # evaluate similarity among the Xs at 
    # a given time node
    M = len(Xs)
    
    #print('node = %s' % (node,))
    Xt = np.array([Xs[k][:,node[k]] for k in range(M)]).T
    Nt = np.array([Ns[k][:,node[k]] for k in range(M)]).T

    den = np.prod(np.linalg.norm(Xt,axis=0))
    return np.sum(np.prod(Xt,axis=1))/max(1, den)

def globalCon(node,nTs):
    M = len(node)

    tmp = np.array(node)
    tmp.sort()
    #rv = (tmp[-1]-tmp[0] <= np.mean(nTs)/2)
    rv = True
    for k in range(M):
        rv = rv and (node[k] < nTs[k]) and (node[k] >= 0)
    
    return rv

def euclidDist(u,v):
    return np.linalg.norm(np.array(v)-np.array(u))

def artItfExp(y,w,itf,fs,tBlk=32e-3,tInc=16e-3,btTime=32e-3,dSIRs=range(-18,6,3),par=True,debug=0):
    # artificial interference
    # y: clean input
    # itf: interference

    # loop over desired SIR (signal interference ratio)
    if par:
        cs = Parallel(n_jobs=8)(delayed(artItfExpPar)(dSIR,y,w,itf,fs,tBlk,tInc,btTime,debug) for dSIR in dSIRs)
    else:
        cs = []
        for dSIR in dSIRs:
            cs.append(artItfExpPar(dSIR,y,w,itf,fs,tBlk,tInc,btTime,debug))

    return np.array(cs)

def artItfExpPar(dSIR,y,w,itf,fs,tBlk,tInc,btTime,debug=0):
    yS,ynS,ff,tt = audio_class.corrupt(y,itf,dSIR,fs,tBlk,tInc)
    wS,wnS,ff,tt = audio_class.corrupt(w,itf,dSIR,fs,tBlk,tInc)

    yX,yN = ridgeTracker(yS,np.median(yS),btTime,tInc)
    wX,wN = ridgeTracker(wS,np.median(wS),btTime,tInc)
    ynX,ynN = ridgeTracker(ynS,np.median(ynS),btTime,tInc)
    wnX,wnN = ridgeTracker(wnS,np.median(wnS),btTime,tInc)

    if debug >= 1:
        plt.figure(figsize=(9, 8), dpi= 80, facecolor='w', edgecolor='k')

        plt.subplot(211)
        im = plt.pcolormesh(tt,ff,np.sqrt(yX))
        plt.autoscale(True,'both',True)
        plt.title('np.shape(yX) = '+str(np.shape(yX)))
        plt.subplot(212)
        im = plt.pcolormesh(tt,ff,np.sqrt(ynX))
        plt.autoscale(True,'both',True)
        plt.title('np.shape(ynX) = '+str(np.shape(ynX)))

        plt.show()

    # no need to create new obj
    # since these are read-only
    TemX =  [yX, yX,  ynX,ynX]
    TemN =  [yN, yN,  ynN,ynN]
    X =     [wnX,ynX, wX, yX]
    N =     [wnN,ynN, wN, yN]
    L = len(TemX)

    Xp = [None]*L
    minDist = [None]*L
    c = [None]*L
    for k in range(L):
        Xp[k],minDist[k],c[k] = dtw([TemX[k],X[k]],[TemN[k],N[k]],1,debug=False)
        c[k] = audio_class.smooth(c[k])

    if debug >= 2:
        for k in range(L):
            plt.figure(figsize=(9, 8), dpi= 80, facecolor='w', edgecolor='k')

            plt.subplot(311)
            plt.pcolormesh(tt,ff,np.sqrt(Xp[k]))
            plt.autoscale(True,'both',True)
            plt.title('np.shape(Xp[%d]) = %s' % (k,np.shape(Xp[k])))
            plt.subplot(312)
            plt.pcolormesh(tt,ff,np.sqrt(X[k]))
            plt.autoscale(True,'both',True)
            plt.title('np.shape(X[%d]) = %s' % (k,np.shape(Xp[k])))
            plt.subplot(313)
            plt.plot(c[k])
            plt.axis([0,len(c[k]),0,1.1])
            #plt.autoscale(True,'both',True)

        plt.show()

    return c
