# Fixed-point ridge tracker for acoustic event detection in C

## Quick start
```
make clean
make
./main input.wav
```
Use test.ipynb to visualize the resulting \*.mat files

## Prerequisites
* Fixed-point FFT: git clone https://github.com/longle2718/kiss_fft
* Fixed-point log: git clone https://github.com/dmoulding/log2fix
* libsndfile: sudo apt-get install libsndfile1-dev 
